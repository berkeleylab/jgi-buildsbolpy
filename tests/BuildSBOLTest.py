'''
Created on Nov 1, 2017

@author: Ernst Oberortner
'''

import unittest

from buildsbol import BuildSBOLAdaptor, BuildSBOLConstants, BUILD_SBOL_TYPES

class Test(unittest.TestCase):


    def test_getConstructsDerivedFromPurchase(self):
        filename = '../data/test/mockup-level-0.sbol.xml'
        buildSBOL = BuildSBOLAdaptor(filename)

        fromPurchaseConstructs = buildSBOL.getConstructsDerivedFromPurchase()
        #self.assertEqual(len(fromPurchaseConstructs), 3)
                
        set1 = buildSBOL.getConstructsFromActivity('purchase')
        set2 = buildSBOL.getConstructsDerivedFromPurchase()
        self.assertEqual(len(set1), len(set2))


    def test_getConstructsDerivedFromArchive(self):
        
        filename = '../data/test/mockup-level-0.sbol.xml'
        buildSBOL = BuildSBOLAdaptor(filename)
        
        fromArchiveConstructs = buildSBOL.getConstructsDerivedFromArchive()
        #self.assertEqual(len(fromArchiveConstructs), 2)        

        set1 = buildSBOL.getConstructsFromActivity('archive')
        set2 = buildSBOL.getConstructsDerivedFromArchive()
        self.assertEqual(len(set1), len(set2))

    def test_getConstructsOfType(self):
        filename = '../data/test/mockup-level-0.sbol.xml'
        buildSBOL = BuildSBOLAdaptor(filename)
        
        ## providing a None type results in an emtpy set
        self.assertIsNot(buildSBOL.getConstructsOfType(), None)
        self.assertEqual(len(buildSBOL.getConstructsOfType()), 0)
        
        ## primers
        for cdType in BUILD_SBOL_TYPES:
            print('type: {}'.format(cdType))
            cds = buildSBOL.getConstructsOfType(cdType)
            
            self.assertIsNot(cds, None)
            #self.assertGreater(len(cds), 0)
            
            for cd in cds:
                print(cd.displayId)
    
    def test_getDeliverables(self):
        filename = '../data/test/mockup-level-0.sbol.xml'
        buildSBOL = BuildSBOLAdaptor(filename)
        
        print('------- <DELIVERABLES> ------')
        deliverables = buildSBOL.getDeliverables()
        print('build process:')
        buildSBOL.printBuildGraph(deliverables)
        print('------- </DELIVERABLES> ------')

    
    def test_setIntersection(self):
        filename = '../data/test/mockup-level-0.sbol.xml'
        buildSBOL = BuildSBOLAdaptor(filename)
        
        ## get the primers
        primers = buildSBOL.getConstructsOfType(BUILD_SBOL_TYPES['PRIMER'])
        constructs_to_purchase =  buildSBOL.getConstructsFromActivity('purchase')
        
        self.assertGreater(len(constructs_to_purchase), len(primers))
        self.assertEqual(len(constructs_to_purchase.intersection(primers)), len(primers))
            
#     def testParseBuildSBOL(self):
#           
#         filename = '/Users/eoberortner/Projects/JGI/BOOST/coding/git/BOOST/data/sbol/jgi/DemoBatch.sbol.xml'
#         buildSBOL = BuildSBOLAdaptor(filename)
#   
#         doc = buildSBOL.document
#           
#         ## the following does not work
#         print('----------------------------------------------------')
#         print('Activities:')
#         for activity in doc.activities:
#             print('    {}'.format(activity))
#         print('----------------------------------------------------')
#   
#         print('----------------------------------------------------')
#         print('Collections:')
#         for collection in doc.collections:
#             print('    {}'.format(collection))
#         print('----------------------------------------------------')
#           
#         ## iterate over the CDs in the document
#         print('----------------------------------------------------')
#         print('ComponentDefinitions:')
#         for cd in doc.componentDefinitions:
#                
#             print('--------------------------')
#             print('Id:        {}'.format(cd))
#             print('displayId: {}'.format(cd.displayId))
#             print('types:')
#             for t in cd.types:
#                 print('    {}'.format(t))
#           
#             print('wasDerivedFrom: {}'.format(cd.getPropertyValue('http://www.w3.org/ns/prov#wasDerivedFrom')))
#             print('properties: {}'.format(cd.getProperties()))
#               
#             ## derivation
#             print('derivation:')
#             if 'https://boost.jgi.doe.gov/derivation' in cd.getProperties():
#                 print('    {}'.format(cd.getPropertyValue('https://boost.jgi.doe.gov/derivation')))
#             else:
#                 print('    N/A')
#   
#             ## JGI types
#             print('JGI type:')
#             if 'https://boost.jgi.doe.gov/type' in cd.getProperties():
#                 print('    {}'.format(cd.getPropertyValue('https://boost.jgi.doe.gov/type')))
#             else:
#                 print('    N/A')
#                   
#             ## wasGeneratedBy
#             print('wasGeneratedBy:')
#             if 'http://www.w3.org/ns/prov#wasGeneratedBy' in cd.getProperties():
#                 if len(cd.getPropertyValue('http://www.w3.org/ns/prov#wasGeneratedBy')) > 0:
#                     print('    {}'.format(cd.getPropertyValue('http://www.w3.org/ns/prov#wasGeneratedBy')))
#                       
# #                     activityId = cd.getPropertyValue('http://www.w3.org/ns/prov#wasGeneratedBy')
# #                     activity = doc.getActivity(activityId)
# #                     print('    activity: {}'.format(activity))
#                       
#                 else:
#                     print('    N/A')
                
        
#     def testReadBuildSBOL(self):
# 
#         filename = '../data/build_sbol/cut_operation.sbol.xml'
#          
#         doc = Document()
#         doc.read(filename)
#  
#  
#         ## iterate over the CDs in the document
#         for cd in doc.componentDefinitions:
#              
#             print('--------------------------')
#             print('displayId: {}'.format(cd.displayId.get()))
#             print('# of types: {}'.format(len(cd.types)))
#  
#             ## the following code works:
#             for i in range(0, len(cd.types)):
#                 
#                 cd_type = cd.types[i]
#                 print('{}-th type: {}'.format((i+1), cd_type))
#                 
#                 p = urlopen(cd_type)
#                 print('path: {}'.format(p))
#  
#         pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()