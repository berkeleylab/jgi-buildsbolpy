'''
Created on Apr 4, 2018

@author: Ernst Oberortner
'''

from sbol import *
from enum import Enum

BUILD_SBOL_TYPES = {'PRIMER' : 'http://identifiers.org/so/SO:0001247', 
                    'LINEAR_CONSTRUCT' : 'http://purl.obolibrary.org/obo/SO_0000987', 
                    'CIRCULAR_CONSTRUCT' : 'http://purl.obolibrary.org/obo/SO_0000988', 
                    'SINGLE_STRANDED' : 'http://purl.obolibrary.org/obo/SO_0000984', 
                    'DOUBLE_STRANDED' : 'http://purl.obolibrary.org/obo/SO_0000985',
                    'RESTRICTION_ENZYME' : 'http://purl.obolibrary.org/obo/GO_0009036'}
    

class BuildSBOLConstants(Enum):
    PURCHASE_URI = 'https://jgi.doe.gov/build/DemoBatch/purchase/1'
    JGI_DELIVERABLE = 'https://boost.jgi.doe.gov/deliverable'
    BUILD_OPERATOR_PROPERTY = 'http://sbolstandard.org/build/operator'

class BuildSBOLAdaptor(object):
    '''
    The BuildSBOLAdaptor provides methods for importing and querying data from Build.SBOL files
    as well as serializing data to Build.SBOL files.
    
    It's build upon the SBOL python library (pysbol).
    
    '''
    
    def __init__(self, build_sbol_filename):

        self.build_sbol_filename = build_sbol_filename

        ## import the file's data
        self.initializeDocument()
            
    def initializeDocument(self):
        """
        The initializeDocument method reads the content of the given file and stores the document of the file
        in the document member variable
        
        :param build_sbol_filename: the filename of the build.SBOL file (path information included)
        
        :return Document: the SBOL document containing all design specifications and the build-instructions
        """
        
        ## instantiate the document
        self.document = Document()

        ## import the document
        self.document.read(self.build_sbol_filename)
        
    
    def getConstructsDerivedFromPurchase(self):
        """
        inspects the document for all component-definitions (CD) that have a "wasDerivedFrom" property 
        that refers to 'purchase' activity
        
        :return: a set of component-definitions that are/need to be purchased 
        """
        return self.getConstructsFromActivity('purchase')
    
            
    def getConstructsDerivedFromArchive(self):
        """
        inspects the document for all component-definitions (CD) that have a "wasDerivedFrom" property 
        that refers to 'purchase' activity
        
        :return: a set of component-definitions that are/need to be purchased 
        """
        return self.getConstructsFromActivity('archive')
    
    ## TODO:
    ## it'll be great to have a "type" of activity, where the type referes to an ontology-term
    ## the ontology-term should indicate the "purchase" type
    def getConstructsFromActivity(self, activityDisplayId):            
        """
        inspects the document for all component-definitions (CD) that have a "wasDerivedFrom" property 
        that refers to 'purchase' activity
        
        :return: a set of component-definitions that are/need to be purchased 
        """
        activityConstructs = set()

        # iterate over the document's component-definitions        
        for cd in self.document.componentDefinitions:
            # check if the CD has a wasDerivedFrom property 
            if len(cd.getPropertyValue(PROVO_WAS_GENERATED_BY)) > 0:
                ## check if the CD's wasDerivedFrom property is a "purchase" activity
                activity = self.document.getActivity(cd.getPropertyValue(PROVO_WAS_GENERATED_BY))
                
                if activity.displayId == activityDisplayId:
                    activityConstructs.add(cd)

        return activityConstructs

    def getConstructsOfType(self, componentDefinitionType = None):
        """
        the getConstructsOfTypes method inspects the document for ComponentDefinitions 
        that have the type as given
        
        :param type: the type of the ComponentDefinition
        
        :return: a set of ComponentDefinitions, each having the required type
        """
        
        type_constructs = set()
        
        if componentDefinitionType not in BUILD_SBOL_TYPES: return type_constructs
        
        ## get the URI of the type
        type_uri = BUILD_SBOL_TYPES[componentDefinitionType]
        
        ## iterate over all CDs
        for cd in self.document.componentDefinitions:
            ## check if the types match
            if type_uri in cd.types:
                ## if so, then store the current CD
                type_constructs.add(cd)
        
        return type_constructs

    def getDeliverables(self):
        """
        
        """
        
        deliverables = set()
        
        ## iterate over all CDs
        for cd in self.document.componentDefinitions:
            ## check if the CD has a jgi:deliverable property
            if BuildSBOLConstants.JGI_DELIVERABLE.value in cd.getProperties():
                ## if so, then store the current CD
                deliverables.add(cd)
        
        return deliverables
       
    def getWasGeneratedByActivity(self, cd):
        if cd.getPropertyValue(PROVO_WAS_GENERATED_BY):
            return self.document.getActivity(cd.getPropertyValue(PROVO_WAS_GENERATED_BY))
    
        return None

    def getComponentDefinition(self, identifier):
        return self.document.getComponentDefinition(identifier)

    def printBuildGraph(self, componentDefinitions, treeLevel = 0):
        """
        traverses and prints the build graph
        
        :param componentDefinitions: a list of CDs from that the traversal should start from
        :param treeLevel: the level of the tree (starting w/ 0) in order to print proper indentations
        """
        
        if componentDefinitions is None: return
        
        for cd in componentDefinitions:
            print('{}ComponentDefinition: {}'.format(treeLevel*4*' ',cd.displayId))
            
            ## check how it was generated
            activity = self.getWasGeneratedByActivity(cd)
            
            if BuildSBOLConstants.BUILD_OPERATOR_PROPERTY.value not in activity.getProperties() and \
                activity.displayId.find('purchase') == -1 and activity.displayId.find('archive') == -1: continue
            
            if BuildSBOLConstants.BUILD_OPERATOR_PROPERTY.value in activity.getProperties():
                print('{}wasGeneratedBy: {}'.format(treeLevel*4*' ', activity.getPropertyValue(BuildSBOLConstants.BUILD_OPERATOR_PROPERTY.value)))
            elif activity.displayId.find('purchase') != -1 or activity.displayId.find('archive') != -1:
                print('{}{}'.format(treeLevel*4*' ', activity.displayId))
            
            
            componentDefinitions = set()
            for usage in activity.usages:
                #print(dir(usage))
                cd = self.getComponentDefinition(usage.entity)
                print('{}usage: {}'.format(treeLevel*4*' ', cd.displayId))
                componentDefinitions.add(cd)
            
            self.printBuildGraph(componentDefinitions, treeLevel+1)


def __main__():
    print('Hello World')    
